import copy

def problem_1():

    data = {}

    with open("day13_input.txt") as data_file:
        for line in data_file:
            l,r = line.strip('\n').replace(": ", ',').split(',')
            data[int(l)] = [int(r), 0, 1]

    num_layers = max(data.keys())
    packet_pos = -1
    total_cost = 0
    for i in range(num_layers + 1):
        # Move packet
        packet_pos += 1

        # Check for collision
        if i in data:
            if data[i][1] == 0:
                #print("collision : ", i)
                total_cost += i * data[i][0]

        #print(i, ":", data)

        # Move scanners
        for j in data:
            pos = data[j][1]
            pos_next = data[j][1] + data[j][2]

            if pos_next == -1:
                data[j][2] = 1
            if pos_next == data[j][0]:
                data[j][2] = -1

            data[j][1] = pos + data[j][2]

    print(total_cost)

def problem_2():

    data = []
    walls = dict()
    idx = 0

    with open("day13_input.txt") as data_file:
        for line in data_file:
            l,r = line.strip('\n').replace(": ", ',').split(',')
            data.append((int(l), int(r), 0, 1))
            walls[int(l)] = idx
            idx += 1
            #data[int(l)] = [int(r), 0, 1]

    num_layers = max(data, key = lambda x : x[0])[0]
        
    def forward(data, steps):
     
        for x in range(steps):
            for j in range(len(data)):
                item = data[j]

                pos = item[2]
                pos_next = item[2] + item[3]
                inc = item[3]

                if pos_next == -1:
                    inc = 1
                if pos_next == item[1]:
                    inc = -1

                pos = pos + inc
                data[j] = (item[0], item[1], pos, inc)

    def simulate(data, delay):
        packet_pos = -1
        total_cost = 0
        for i in range(num_layers + 1):
            # Move packet
            packet_pos += 1

            # Check for collision
            if i in walls:
                if data[walls[i]][2] == 0:
                    return 1
                    #print("collision : ", i)

            #print(i, ":", data)
            forward(data, 1)
            
        return 0

    delay = 0
    base_data = data[:]
    while 1:       
        cost = simulate(base_data[:], delay)
        if delay % 1000 == 0:
            print("delay %ld = %ld" % (delay, cost))
        if cost == 0:
            print("Min delay %ld" % delay)
            break
        delay -= 1
        forward(base_data, 1)
        



if __name__ == '__main__':
    problem_2()