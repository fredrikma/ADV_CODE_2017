def knot_hash(lengths):
    data = [x for x in range(256)]

    ascii = []
    for c in lengths:
        ascii.append(ord(c))
    ascii += [17, 31, 73, 47, 23]
    lengths = ascii

    n = len(data)
    skip_size = 0
    curr_pos = 0

    def round(lengths, curr_pos, skip_size):
        while len(lengths) > 0:
            l = lengths[0]

            selection = []
            for i in range(l):
                idx = (curr_pos + i) % n
                selection.append(data[idx])

            selection = selection[::-1]
            for i in range(l):
                idx = (curr_pos + i) % n
                data[idx] = selection[i]

       
            curr_pos += l + skip_size
            lengths = lengths[1:]
            skip_size += 1
        return (curr_pos, skip_size)

    for r in range(64):
        curr_pos, skip_size = round(lengths[:], curr_pos, skip_size)

    # reduce
    sums = []
    for i in range(16):
        block = data[(i*16):(i*16+16)]
        sum = block[0]
        for j in range(1,16):
            sum ^= block[j]
        sums.append(sum)

    #print(sums)

    result = ""
    for c in sums:
        i = hex(c)
        d = i[2:]
        if len(d) == 1:
            d = "0" + d[0]
        result += d

    #print(result)
    #print(len(result))
    return result

def problem_1():
    used = 0
    for row in range(128):
        key = "jzgqcdpd-" + str(row)
        hash = knot_hash(key)
        n = int(hash, 16)
        used += bin(n).count("1")

    print(used)

def problem_2():

    grid = [0 for x in range(130 * 130)]
    for row in range(128):
        key = "jzgqcdpd-" + str(row)
        #key = "flqrgnkx-" + str(row)
        hash = knot_hash(key)
        n = int(hash, 16)

        b = bin(n)[2:]
        prepend = "".join(['0' for x in range(128 - len(b))])
        b = prepend + b

        for i,d in enumerate(b):
            grid[(row + 1) * 130 + i + 1] = int(d)

    def flood(grid, x, y, value):
        if grid[y * 130 + x] != 1:
            return

        grid[y * 130 + x] = value
        flood(grid, x - 1, y, value)
        flood(grid, x + 1, y, value)
        flood(grid, x, y - 1, value)
        flood(grid, x, y + 1, value)
    
    region = 1
    for y in range(130):
        for x in range(130):
            if grid[y * 130 + x] == 1:
                region += 1
                flood(grid, x, y, region)


    print(max(grid) - 1)

if __name__ == '__main__':
    problem_1()
    problem_2()
