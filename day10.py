
def problem_1():
    lengths = [183,0,31,146,254,240,223,150,2,206,161,1,255,232,199,88]
    data = [x for x in range(256)]
    #lengths = [3,4,1,5]
    #data = [x for x in range(5)]

    n = len(data)
    skip_size = 0
    curr_pos = 0

    while len(lengths) > 0:
        l = lengths[0]

        selection = []
        for i in range(l):
            idx = (curr_pos + i) % n
            selection.append(data[idx])

        selection = selection[::-1]
        for i in range(l):
            idx = (curr_pos + i) % n
            data[idx] = selection[i]

       
        curr_pos += l + skip_size
        lengths = lengths[1:]
        skip_size += 1

    print(data)
    print(data[0] * data[1])

def problem_2():
    #lengths = """183,0,31,146,254,240,223,150,2,206,161,1,255,232,199,88"""
    lengths = """183,0,31,146,254,240,223,150,2,206,161,1,255,232,199,88"""
    data = [x for x in range(256)]
    #lengths = """1,2,3"""

    ascii = []
    for c in lengths:
        ascii.append(ord(c))
    ascii += [17, 31, 73, 47, 23]
    lengths = ascii

    n = len(data)
    skip_size = 0
    curr_pos = 0

    def round(lengths, curr_pos, skip_size):
        while len(lengths) > 0:
            l = lengths[0]

            selection = []
            for i in range(l):
                idx = (curr_pos + i) % n
                selection.append(data[idx])

            selection = selection[::-1]
            for i in range(l):
                idx = (curr_pos + i) % n
                data[idx] = selection[i]

       
            curr_pos += l + skip_size
            lengths = lengths[1:]
            skip_size += 1
        return (curr_pos, skip_size)

    for r in range(64):
        curr_pos, skip_size = round(lengths[:], curr_pos, skip_size)
    
    #print(data)

    # reduce
    sums = []
    for i in range(16):
        block = data[(i*16):(i*16+16)]
        sum = block[0]
        for j in range(1,16):
            sum ^= block[j]
        sums.append(sum)

    print(sums)

    result = ""
    for c in sums:
        i = hex(c)
        d = i[2:]
        if len(d) == 1:
            d = "0" + d[0]
        result += d

    print(result)
    print(len(result))

if __name__ == '__main__':
    problem_1()
    problem_2()