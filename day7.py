def problem_1():
    bases = []
    aboves = []
    with open("day7_input.txt") as data_file:
        for row in data_file:
            data = row.strip('\n').split('->')
            base_lst = data[0].replace('(', '').replace(')', '').split(' ')
            bases.append(base_lst[0])
            
            if len(data) > 1:
                above_lst = data[1].replace(',', '').strip().split(' ')
                aboves += above_lst

    r = list(filter(lambda x: x not in aboves, bases))
    print(r)

def problem_2():
    weights = dict()
    
    bases = []   #name
    aboves = []  #(name, [children])
    with open("day7_input.txt") as data_file:
        for row in data_file:
            data = row.strip('\n').split('->')
            base_lst = data[0].replace('(', '').replace(')', '').split(' ')
            bases += base_lst[0]

            weights[base_lst[0]] = int(base_lst[1])
            
            above_lst = []
            if len(data) > 1:
                above_lst = data[1].replace(',', '').strip().split(' ')
            aboves.append((base_lst[0], above_lst))


    org_weights = dict(weights)
    # we know the base, build the tree ordering from there
    # fbgguv
    tree = ['fbgguv']

    def build_tree(name, tree):
        record = list(filter(lambda x: x[0] == name, aboves))
        #print(record[0][0])
        #tree.append(record[0][0])

        for child in record[0][1]:
            #print(child)
            tree.append(child)
        for child in record[0][1]:
            build_tree(child, tree)

    build_tree('fbgguv', tree)

    # calculate weights from top
    tree = tree[::-1]
    for name in tree:
        record = list(filter(lambda x: x[0] == name, aboves))
        weight = weights[record[0][0]]

        for child in record[0][1]:
            weight += weights[child]

        weights[record[0][0]] = weight

    # weights ok now check from bottom
    def check_weights(node):
        print(node)
        if len(node[1]) < 1:
            return
        w = -1
        for child in node[1]:
            print("%s : %ld" % (child, org_weights[child]))
            if w == -1:
                w = weights[child]

            if w != weights[child]:
                print("%ld, %ld, %ld" % (w, weights[child], w - weights[child]))
                print("Needs to be %ld" % (org_weights[child] + (w - weights[child])))
                r = list(filter(lambda x: x[0] == child, aboves))
                check_weights(r[0])
                break



    r = list(filter(lambda x: x[0] == 'fbgguv', aboves))
    check_weights(r[0])


if __name__ == '__main__':
    problem_2()