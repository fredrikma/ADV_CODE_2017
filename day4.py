def problem_1():
    content = ""
    with open("day4_input.txt") as data_file:
        content = data_file.read()

    content = content.split('\n')
    num_valid = 0
    for line in content:
        invalid = False
        words = line.split()
        for word in words:
            if words.count(word) != 1:
                invalid = True
                break

        if not invalid:
            num_valid = num_valid + 1
             

    print(num_valid)

def problem_2():
    content = ""
    with open("day4_input.txt") as data_file:
        content = data_file.read()

    content = content.split('\n')
    num_valid = 0
    for line in content:
        words = line.split()

        def check_words(words):
            for i_w in range(len(words) - 1):
                word1 = words[i_w]
                for j_w in range(i_w + 1, len(words)):
                    word2 = words[j_w]

                    if len(word1) == len(word2):
                        num_matching = 0
                        for char in word1:
                            if char in word2:
                                num_matching = num_matching + 1
                        if num_matching == len(word1):
                            return True
            return False

        if not check_words(words):
            num_valid = num_valid + 1
             

    print(num_valid)

if __name__ == '__main__':
    problem_1()
    problem_2()