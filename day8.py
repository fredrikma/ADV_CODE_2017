def problem_1_2():
    registers = dict()
    operators = dict() #
    
    cond_ne = lambda a,b: a != b
    cond_g = lambda a,b: a > b
    cond_l = lambda a,b: a < b
    cond_ge = lambda a,b: a >= b
    cond_le = lambda a,b: a <= b
    cond_e = lambda a,b: a == b

    inc = lambda a, b: a + b
    dec = lambda a, b: a - b
    operators['inc'] = inc
    operators['dec'] = dec


    conditions = [('!=', cond_ne),
                  ('>=', cond_ge),
                  ('<=', cond_le),
                  ('>', cond_g),
                  ('<', cond_l),
                  ('==', cond_e)]
    
    highest = 0
    with open("day8_input.txt") as data_file:
        for line in data_file:
            line = line.strip('\n')
            ab = line.split('if')
            opers = ab[0].split(' ')
            cond = ab[1]

            for c in conditions:
                if c[0] in cond:
                    op = c[1]
                    regs = cond.strip().split(c[0])
                    reg_1 = regs[0].strip()
                    const = int(regs[1])
                    break

            def get_reg_value(r):
                if r not in registers:
                    registers[r] = 0
                return registers[r]
            
            if op(get_reg_value(reg_1), const):
                new_value = operators[opers[1]](get_reg_value(opers[0]), int(opers[2]))
                registers[opers[0]] = new_value
                highest = max(highest, new_value)


    #print(registers)

    print(max(registers.items(), key=lambda k: k[1]))
    print(highest)

if __name__ == '__main__':
    problem_1_2()