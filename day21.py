import math

def problem_12():
    #data_file = "../.# => ##./#../...\n.#./..#/### => #..#/..../..../#..#"

    with open("day21_input.txt", "r") as file:
        data_file = file.read()

    def flip_x(a):
        r = []
        for row in a:
            r.append(row[::-1])
        return r

    def flip_y(a):
        r = []
        for row in a[::-1]:
            r.append(row)
        return r

    def rotate(a):
        r = list(zip(*a[::-1]))
        return r
        
    pattern_2x2 = []
    input = [(0,1),
             (2,3)]
    for i in range(4):
        pattern_2x2.append(flip_x(input))
        pattern_2x2.append(flip_y(input))
        input = rotate(input)
        pattern_2x2.append(input)

    pattern_3x3 = []
    input = [(0,1,2),
             (3,4,5),
             (6,7,8)]
    for i in range(4):
        pattern_3x3.append(flip_x(input))
        pattern_3x3.append(flip_y(input))
        input = rotate(input)
        pattern_3x3.append(input)

    from_to = {}

    data_file = data_file.splitlines()
    for line in data_file:
        f, t = [x.strip().replace('/', '') for x in line.split('=>')]

        patterns = pattern_2x2
        if int(math.sqrt(len(f))) == 3:
            patterns = pattern_3x3

        for p in patterns:
            key = ""
            for i in list(sum(p, ())):
                key += f[i]
            from_to[key] = t

    progs = ".#...####"
    for i in range(18):

        def enhance(prog, from_to):
            w = int(math.sqrt(len(prog)))
            if w % 2 == 0:
                sz = 2
            else:
                sz = 3

            result_progs = {}
            d = w // sz
            for oy in range(d):
                for ox in range(d):
                    base_idx = oy * sz * w + ox * sz

                    sub_prog = ""
                    for iy in range(sz):
                        for ix in range(sz):
                            sub_prog += prog[base_idx + iy * w + ix]

                    sub_prog = from_to[sub_prog]
                    result_progs[(ox,oy)] = sub_prog[:]

            sz = int(math.sqrt(len(result_progs[(0,0)])))
            image = [0 for x in range(d * d * sz * sz)]
            # join
            for oy in range(d):
                for ox in range(d):
                    base_idx = oy * d * sz * sz + ox * sz
                    sub_img = result_progs[(ox,oy)]
                    
                    for iy in range(sz):
                        for ix in range(sz):
                            image[base_idx + iy * d * sz + ix] = sub_img[iy * sz + ix]
            return image

        r = enhance(progs, from_to)
        progs = "".join(r)
        if i == 4:
            print(progs.count('#'))

    print(progs.count('#'))


if __name__ == '__main__':
    problem_12()