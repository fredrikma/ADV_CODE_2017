with open("day9_input.txt") as data_file:
    data = data_file.read()

#data = "<{!>}>"

group_points = 0
total_points = 0
total_garbage = 0

garbage = False
skipnext = False
for c in data:

    if skipnext:
        skipnext = False
        continue

    if c == "!":
        skipnext = True
        continue

    if c == "<" and not garbage:
        garbage = True
        continue
    if garbage and c == ">":
        garbage = False
        continue

    if garbage:
        total_garbage += 1
        continue

    if c == "{":
        group_points += 1
    elif c == "}":
        total_points += group_points
        group_points -= 1

print(total_points)
print(total_garbage)

