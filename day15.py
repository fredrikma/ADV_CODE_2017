from collections import deque

def problem_1():
    gen_a = 618
    gen_b = 814

    matches = 0
    cnt = 40e6
    while cnt > 0:
        gen_a *= 16807
        gen_b *= 48271

        gen_a %= 2147483647
        gen_b %= 2147483647

        if (gen_a & 0xffff) == (gen_b & 0xffff):
            matches += 1

        cnt -= 1

    print("matches %ld" % matches)

def problem_2():
    gen_a = 618
    gen_b = 814

    matches = 0
    cmp_a = deque()
    cmp_b = deque()
    cnt = 0
    while cnt < 5e6:
        gen_a *= 16807
        gen_b *= 48271

        gen_a %= 2147483647
        gen_b %= 2147483647

        if gen_a % 4 == 0:
            cmp_a.append(gen_a)
        if gen_b % 8 == 0:
            cmp_b.append(gen_b)

        if len(cmp_a) > 0 and len(cmp_b) > 0:
            a = cmp_a.popleft()
            b = cmp_b.popleft()
            cnt += 1

            if (a & 0xffff) == (b & 0xffff):
                matches += 1

        if cnt % 100000 == 0:
            print(cnt, matches)

    print("matches %ld" % matches)

if __name__ == '__main__':
    problem_2()