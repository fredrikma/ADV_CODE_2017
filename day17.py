def problem_1():
    step = 380
    buffer = [0]

    curr_idx = 0
    cnt = 1
    while cnt <= 2017:
        idx = (curr_idx + step) % len(buffer)
        buffer.insert(idx + 1, cnt)
        curr_idx = idx + 1
        cnt += 1

    print(buffer[(curr_idx-2):(curr_idx+2)])
    print(buffer)
    
def problem_2():
    step = 380
    buffer = [0]

    curr_idx = 0
    cnt = 1
    after_zero = 1
    while cnt <= 50000000:
        idx = (curr_idx + step) % cnt

        if idx == 0:
            after_zero = cnt

        curr_idx = idx + 1
        cnt += 1

    print(after_zero)

if __name__ == '__main__':
    problem_1()
    problem_2()