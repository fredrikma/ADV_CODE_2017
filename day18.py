def problem_1():

    program = []
    ip = 0
    regs = dict([(chr(x), 0) for x in range(ord('a'), ord('z') + 1)])
    with open("day18_input.txt", "r") as data_file:
        for line in data_file:
            instr, op_a, *op_b = line.strip('\n').split(' ')
            program.append((instr, op_a, op_b))

    def f_snd(x, y):
        if x in regs:
            x = regs[x]
        regs['snd_reg'] = int(x)
        return 1

    def f_rcv(x, y):
        if 'snd_reg' in regs:
            y = regs['snd_reg']
        if y != 0:
            print("rcv %ld" % y)
        return 1

    def f_set(x, y):
        if y in regs:
            y = regs[y]
        regs[x] = int(y)
        return 1

    def f_add(x, y):
        if y in regs:
            y = regs[y]
        regs[x] += int(y)
        return 1

    def f_mul(x, y):
        if y in regs:
            y = regs[y]
        regs[x] *= int(y)
        return 1

    def f_mod(x, y):
        if y in regs:
            y = regs[y]
        regs[x] %= int(y)
        return 1

    def f_jgz(x, y):
        if x not in regs:
            return 1
        if regs[x] <= 0:
            return 1
        return int(y)

    f_map = dict([('snd', f_snd),
                  ('rcv', f_rcv),
                  ('set', f_set),
                  ('add', f_add),
                  ('mul', f_mul),
                  ('mod', f_mod),
                  ('jgz', f_jgz)])
    while ip >= 0 and ip < len(program):
        instr, op_a, op_b = program[ip]
        if len(op_b) > 0:
            op_b = op_b[0]
        ip += f_map[instr](op_a, op_b)

send_1 = 0

def problem_2():

    program = []
    ip_0 = 0
    ip_1 = 0
    regs_0 = dict([(chr(x), 0) for x in range(ord('a'), ord('z') + 1)])
    regs_1 = dict([(chr(x), 0) for x in range(ord('a'), ord('z') + 1)])
    regs_1['p'] = 1

    regs_0['prog'] = 0
    regs_1['prog'] = 1

    rcv_0 = []
    rcv_1 = []

    with open("day18_input.txt", "r") as data_file:
        for line in data_file:
            instr, op_a, *op_b = line.strip('\n').split(' ')
            program.append((instr, op_a, op_b))

#    data = "snd 1\nsnd 2\nsnd p\nrcv a\nrcv b\nrcv c\nrcv d"
#    data = data.splitlines()
#    for line in data:
#        instr, op_a, *op_b = line.strip('\n').split(' ')
#        program.append((instr, op_a, op_b))

    def f_snd(x, y, regs):
        global send_1
        if x in regs:
            x = regs[x]

        if regs['prog'] == 0:
            rcv_1.append(int(x))
        else:
            rcv_0.append(int(x))
            send_1 += 1
        #print("send %ld %ld" % (len(rcv_0), len(rcv_1)))
        return 1

    def f_rcv(x, y, regs):
        if regs['prog'] == 0:
            if len(rcv_0) == 0:
                #print("deadlock 0?")
                return 0
            else:
                y = rcv_0.pop(0)
        else:
            if len(rcv_1) == 0:
                #print("deadlock 1?")
                return 0
            else:
                y = rcv_1.pop(0)
        regs[x] = int(y)
        return 1

    def f_set(x, y, regs):
        if y in regs:
            y = regs[y]
        regs[x] = int(y)
        return 1

    def f_add(x, y, regs):
        if y in regs:
            y = regs[y]
        regs[x] += int(y)
        return 1

    def f_mul(x, y, regs):
        if y in regs:
            y = regs[y]
        regs[x] *= int(y)
        return 1

    def f_mod(x, y, regs):
        if y in regs:
            y = regs[y]
        regs[x] %= int(y)
        return 1

    def f_jgz(x, y, regs):
        if x == '1':
            return int(y)
            
        if regs[x] <= 0:
            return 1
        if y in regs:
            y = regs[y]
        return int(y)

    f_map = dict([('snd', f_snd),
                  ('rcv', f_rcv),
                  ('set', f_set),
                  ('add', f_add),
                  ('mul', f_mul),
                  ('mod', f_mod),
                  ('jgz', f_jgz)])

    while ip_0 >= 0 and ip_0 < len(program) and ip_1 >= 0 and ip_1 < len(program):
        # first prog to rcv
        while 1:
            instr, op_a, op_b = program[ip_0]
            if instr == 'rcv':
                break
            if len(op_b) > 0:
                op_b = op_b[0]
            ip_0 += f_map[instr](op_a, op_b, regs_0)
        
        # second prog to rcv
        while 1:
            instr, op_a, op_b = program[ip_1]
            if instr == 'rcv':
                break
            if len(op_b) > 0:
                op_b = op_b[0]
            ip_1 += f_map[instr](op_a, op_b, regs_1)

        # first prog rcv
        instr, op_a, op_b = program[ip_0]
        if len(op_b) > 0:
            op_b = op_b[0]
        ip_0 += f_map[instr](op_a, op_b, regs_0)

        # second prog rcv
        instr, op_a, op_b = program[ip_1]
        if len(op_b) > 0:
            op_b = op_b[0]
        ip_1 += f_map[instr](op_a, op_b, regs_1)

        if len(rcv_0) == 0 and len(rcv_1) == 0:
            print(send_1)
            break



if __name__ == '__main__':
    problem_2()