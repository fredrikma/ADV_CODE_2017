def problem_1():
    ops = []
    with open("day16_input.txt", "r") as data_file:
        ops = data_file.read().split(',')

    #ops = "s1,x3/4,pe/b".split(',')

    progs = [ord('a') + x for x in range(16)]
    for op in ops:
        if op[0] == 's':
            r = -int(op[1:]) % len(progs)
            progs = progs[r:] + progs[:r]

        elif op[0] == 'x':
            i, j = (int(x) for x in op[1:].split('/'))
            progs[j], progs[i] = progs[i], progs[j]

        elif op[0] == 'p':
            a, b = (x for x in op[1:].split('/'))
            i = progs.index(ord(a))
            j = progs.index(ord(b))
            progs[j], progs[i] = progs[i], progs[j]

    print("".join([chr(x) for x in progs]))

def problem_2():
    ops = []
    with open("day16_input.txt", "r") as data_file:
        ops = data_file.read().split(',')

    progs = [ord('a') + x for x in range(16)]

    opt = []
    for op in ops:
        if op[0] == 's':
            r = -int(op[1:]) % len(progs)
            opt.append((0,r))
        elif op[0] == 'x':
            i, j = (int(x) for x in op[1:].split('/'))
            opt.append((1,i,j))
        elif op[0] == 'p':
            a, b = (x for x in op[1:].split('/'))
            opt.append((2,ord(a),ord(b)))

    res = []
    period = 0
    for k in range(121):
        for op in opt:
            if op[0] == 0:
                progs = progs[op[1]:] + progs[:op[1]]
            elif op[0] == 1:
                i, j = op[1], op[2]
                progs[j], progs[i] = progs[i], progs[j]
            elif op[0] == 2:
                a, b = op[1], op[2]
                i = progs.index(a)
                j = progs.index(b)
                progs[j], progs[i] = progs[i], progs[j]

        r = "".join([chr(x) for x in progs])
        #print(r)

        if r in res:
            print("match %ld" % period)
            break

        res.append(r)
        period += 1

    idx = 1000000000 % period
    print(res[idx-1])

if __name__ == '__main__':
    problem_1()
    problem_2()