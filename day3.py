import math
import numpy as np


def problem_1():

    value = 289326

    # find area of quad
    s_i = math.floor(math.sqrt(value))
    if s_i % 2 == 0:
        s_i = s_i - 1
    print(s_i)

    area = s_i**2
    diff = value - area
    print("diff: %ld" % diff)
    
    curr_val = area

    a_s = s_i + 1
    a_t = s_i + 2

    if curr_val + a_s >= value:
        print("Right side")
        end_value = curr_val + a_s
        center_value = end_value - a_s / 2

        steps_to_center = math.fabs(value - center_value)
        steps_to_one = (s_i + 1) / 2
        print("total steps %f" % (steps_to_center + steps_to_one))

        pass

    elif curr_val + a_s + a_t >= value:
        print("Top side")
        end_value = curr_val + a_s + a_t
        center_value = end_value - (a_t + 1) / 2

        steps_to_center = math.fabs(value - center_value)
        steps_to_one = (s_i + 1) / 2
        print("total steps %f" % (steps_to_center + steps_to_one))
    elif curr_val + a_s + a_t + a_s >= value:
        print("Left side")
    elif curr_val + a_s + a_t + a_s + a_t >= value:
        print("Bottom side")


def problem_2():
    value = 289326

    s_i = 10

    matrix = np.zeros([s_i,s_i], dtype = int)


    curr_x = (s_i - 1) // 2
    curr_y = (s_i - 1) // 2

    matrix[curr_y, curr_x] = 1

    def check_value(v):
        if v > value:
            print("Found %ld" % v)

    ru_step = 1
    ld_step = 2
    num_elements = 0
    while num_elements < (s_i - 3)**2:
        
        for x in range(ru_step):
            num_elements = num_elements + 1
            curr_x = curr_x + 1
            matrix[curr_y, curr_x] = (matrix[curr_y - 1, curr_x - 1] + matrix[curr_y - 1, curr_x] + matrix[curr_y - 1, curr_x + 1] +
                                      matrix[curr_y + 1, curr_x - 1] + matrix[curr_y + 1, curr_x] + matrix[curr_y + 1, curr_x + 1] +
                                      matrix[curr_y, curr_x - 1] + matrix[curr_y, curr_x + 1])
            check_value(matrix[curr_y, curr_x])


        for y in range(ru_step):
            num_elements = num_elements + 1
            curr_y = curr_y - 1
            matrix[curr_y, curr_x] = (matrix[curr_y - 1, curr_x - 1] + matrix[curr_y - 1, curr_x] + matrix[curr_y - 1, curr_x + 1] +
                                      matrix[curr_y + 1, curr_x - 1] + matrix[curr_y + 1, curr_x] + matrix[curr_y + 1, curr_x + 1] +
                                      matrix[curr_y, curr_x - 1] + matrix[curr_y, curr_x + 1])
            check_value(matrix[curr_y, curr_x])

        for x in range(ld_step):
            num_elements = num_elements + 1
            curr_x = curr_x - 1
            matrix[curr_y, curr_x] = (matrix[curr_y - 1, curr_x - 1] + matrix[curr_y - 1, curr_x] + matrix[curr_y - 1, curr_x + 1] +
                                      matrix[curr_y + 1, curr_x - 1] + matrix[curr_y + 1, curr_x] + matrix[curr_y + 1, curr_x + 1] +
                                      matrix[curr_y, curr_x - 1] + matrix[curr_y, curr_x + 1])
            check_value(matrix[curr_y, curr_x])

        for y in range(ld_step):
            num_elements = num_elements + 1
            curr_y = curr_y + 1
            matrix[curr_y, curr_x] = (matrix[curr_y - 1, curr_x - 1] + matrix[curr_y - 1, curr_x] + matrix[curr_y - 1, curr_x + 1] +
                                      matrix[curr_y + 1, curr_x - 1] + matrix[curr_y + 1, curr_x] + matrix[curr_y + 1, curr_x + 1] +
                                      matrix[curr_y, curr_x - 1] + matrix[curr_y, curr_x + 1])
            check_value(matrix[curr_y, curr_x])

        ru_step = ru_step + 2
        ld_step = ld_step + 2



if __name__ == '__main__':
    problem_1()
    problem_2()
