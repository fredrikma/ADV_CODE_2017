d_u = 1 << 0
d_d = 1 << 1
d_l = 1 << 2
d_r = 1 << 3

def problem_12():
    maze = []

    with open("day19_input.txt", "r") as data_file:
        for row in data_file:
            maze.append(row.strip('\n'))

    curr_pos = [1,0]
    curr_dir = d_d
    chars = []
    steps = 1
    while curr_dir != 0:
        if curr_dir == d_d:
            offset = [0,1]
        elif curr_dir == d_u:
            offset = [0,-1]
        elif curr_dir == d_l:
            offset = [-1,0]
        else:
            offset = [1,0]

        while maze[curr_pos[1]][curr_pos[0]] != ' ':
            c = maze[curr_pos[1]][curr_pos[0]]
            if not c in ['|','-','+']:
                chars.append(c)
                print(chars)
            curr_pos[0] += offset[0]
            curr_pos[1] += offset[1]
            steps += 1

        curr_pos[0] -= offset[0]
        curr_pos[1] -= offset[1]
        steps -= 1
                
        if curr_dir == d_u or curr_dir == d_d:
            if maze[curr_pos[1]][curr_pos[0]-1] != ' ':
                curr_dir = d_l
            elif maze[curr_pos[1]][curr_pos[0]+1] != ' ':
                curr_dir = d_r
            else:
                curr_dir = 0
        else:
            if maze[curr_pos[1]-1][curr_pos[0]] != ' ':
                curr_dir = d_u
            elif maze[curr_pos[1]+1][curr_pos[0]] != ' ':
                curr_dir = d_d
            else:
                curr_dir = 0

    print("".join(chars))
    print(steps)


if __name__ == '__main__':
    problem_12()