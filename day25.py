tape = []

def problem_1():
    
    curr_idx = 0
    num_steps = 12459852 
    state = 'A'

    def extend_tape(idx):
        global tape
        if idx == -1:
            tape = [0] + tape
            return 0
        elif idx == len(tape):
            tape = tape + [0]

        return idx
        

    for step in range(num_steps):
        curr_idx = extend_tape(curr_idx)
        curr_val = tape[curr_idx]

        if state == 'A':
            if curr_val == 0:
                tape[curr_idx] = 1
                curr_idx += 1
                state = 'B'
            else:
                tape[curr_idx] = 1
                curr_idx -= 1
                state = 'E'
        elif state == 'B':
            if curr_val == 0:
                tape[curr_idx] = 1
                curr_idx += 1
                state = 'C'
            else:
                tape[curr_idx] = 1
                curr_idx += 1
                state = 'F'
        elif state == 'C':
            if curr_val == 0:
                tape[curr_idx] = 1
                curr_idx -= 1
                state = 'D'
            else:
                tape[curr_idx] = 0
                curr_idx += 1
                state = 'B'
        elif state == 'D':
            if curr_val == 0:
                tape[curr_idx] = 1
                curr_idx += 1
                state = 'E'
            else:
                tape[curr_idx] = 0
                curr_idx -= 1
                state = 'C'
        elif state == 'E':
            if curr_val == 0:
                tape[curr_idx] = 1
                curr_idx -= 1
                state = 'A'
            else:
                tape[curr_idx] = 0
                curr_idx += 1
                state = 'D'
        elif state == 'F':
            if curr_val == 0:
                tape[curr_idx] = 1
                curr_idx += 1
                state = 'A'
            else:
                tape[curr_idx] = 1
                curr_idx += 1
                state = 'C'

    print(tape.count(1))


if __name__ == '__main__':
    problem_1()