input = [11, 11, 13, 7, 0, 15, 5, 5, 4, 4, 1, 1, 7, 1, 15, 11]

def problem_1():

    seen = []
    num_iters = 0

    while True:
        #print(input)
        max_idx, max_blocks = max(enumerate(input), key = lambda x: x[1])

        input[max_idx] = 0
        while max_blocks > 0:
            max_idx = (max_idx + 1) % len(input)
            input[max_idx] = input[max_idx] + 1
            max_blocks = max_blocks - 1

        num_iters = num_iters + 1
        if input in seen:
            break

        seen.append(input[:])

    print(num_iters)

def problem_2():

    seen = []
    num_iters = 0

    while True:
        #print(input)
        max_idx, max_blocks = max(enumerate(input), key = lambda x: x[1])

        input[max_idx] = 0
        while max_blocks > 0:
            max_idx = (max_idx + 1) % len(input)
            input[max_idx] = input[max_idx] + 1
            max_blocks = max_blocks - 1

        num_iters = num_iters + 1
        if input in seen:
            idx = seen.index(input)
            print(num_iters - idx - 1)
            break

        seen.append(input[:])
    

if __name__ == '__main__':
    problem_1()
    problem_2()