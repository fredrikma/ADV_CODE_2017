def problem_1():

    data = dict()
    with open("day12_input.txt") as data_file:
    #with open("day12_small.txt") as data_file:
        for line in data_file:
            tokens = line.strip('\n').replace(' ', '').split('<->')
            id = int(tokens[0])
            pipes = [int(x) for x in tokens[1].split(',')]
            data[id] = pipes

    visited = []
    #reachable = []
    curr_id = 0

    def travel(curr_id):
        visited.append(curr_id)
        for connected in data[curr_id]:
            if not connected in visited:
                travel(connected)

    travel(0)
    print(len(visited))

def problem_2():

    data = dict()
    with open("day12_input.txt") as data_file:
    #with open("day12_small.txt") as data_file:
        for line in data_file:
            tokens = line.strip('\n').replace(' ', '').split('<->')
            id = int(tokens[0])
            pipes = [int(x) for x in tokens[1].split(',')]
            data[id] = pipes

    visited = []
    not_visited = [x for x in range(2000)]
    #not_visited = [x for x in range(7)]
    num_groups = 0
    curr_id = 0

    def travel(curr_id):
        visited.append(curr_id)
        not_visited.remove(curr_id)
        for connected in data[curr_id]:
            if not connected in visited:
                travel(connected)

    while len(not_visited) > 0:
        travel(not_visited[0])
        num_groups += 1

    print(num_groups)

if __name__ == '__main__':
    problem_1()
    problem_2()