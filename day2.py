
def problem_1():
    
    with open("day2_input_1.txt") as data_file:
        content = data_file.read()

    content = content.split('\n')
    content = [[int(y) for y in x.split('\t')] for x in content]

    total = 0
    for row in content:
        total = total + max(row) - min(row)

    print(total)

def problem_2():
   
    with open("day2_input_1.txt") as data_file:
        content = data_file.read()

    content = content.split('\n')
    content = [sorted([int(y) for y in x.split('\t')], reverse = True) for x in content]
    
    total = 0
    for row in content:
        n = len(row)
        for i in range(n - 1):
            for j in range(i + 1, n):
                if row[i] % row[j] == 0:
                    total = total + row[i] // row[j]

    print(total)

if __name__ == '__main__':
    problem_1()
    problem_2()