def problem(instructions, jmp_func):
    ip = 0
    num_jmps = 0
    while ip >= 0 and ip < len(instructions):
        next_ip_offset = instructions[ip]
        instructions[ip] = instructions[ip] + jmp_func(next_ip_offset)

        num_jmps = num_jmps + 1
        ip = ip + next_ip_offset
     
    return num_jmps

if __name__ == '__main__':
    with open("day5_input.txt") as data_file:
        content = data_file.read()
    instructions = [int(x) for x in content.split('\n')]

    print(problem(instructions[:], lambda x: 1))
    print(problem(instructions[:], lambda x: -1 if  x >= 3 else 1))
