def problem_12():

    with open("day11_input.txt") as data_file:
        moves = data_file.read().split(',')

    #moves = "ne,ne,sw,sw".split(',')

    table = dict([['n', (1,-1,0)], ['ne', (0,-1,1)], ['se', (-1,0,1)],
                  ['s', (-1,1,0)], ['sw', (0,1,-1)], ['nw', (1,0,-1)]])

    
    curr_coord = (0,0,0)

    def add_ab(a, b):
        return (a[0]+b[0], a[1]+b[1], a[2]+b[2])

    max_d = 0

    for m in moves:
        curr_coord = add_ab(curr_coord, table[m])   
        max_d = max(max_d, sum(map(lambda x: abs(x), curr_coord)) // 2)

    print(curr_coord)
    
    d = sum(map(lambda x: abs(x), curr_coord)) // 2

    print(d)
    print(max_d)

if __name__ == '__main__':
    problem_12()