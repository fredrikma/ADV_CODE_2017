longest = 0
strongest = 0

def problem_1():
    with open("day24_input.txt", "r") as data_file:
        components = data_file.read().splitlines()

    def recurse(curr_str, p1, p2, components):
        global strongest
        curr_str += int(p1) + int(p2)

        if curr_str > strongest:
            print(p1, p2)
            strongest = curr_str
            print(strongest)

        for c in components:
            ports = c.split('/')
                    
            if p2 in ports:
                avail = components[:]
                avail.remove(c)

                n1, n2 = ports
                if n1 != p2:
                    n1, n2 = n2, n1

                recurse(curr_str, n1, n2, avail)

    for c in components:
        if '0' in c.split('/'):
            avail = components[:]
            avail.remove(c)

            p1, p2 = c.split('/')
            if p1 != '0':
                p1, p2 = p2, p1

            recurse(0, p1, p2, avail)

    print(strongest)

def problem_2():
    with open("day24_input.txt", "r") as data_file:
        components = data_file.read().splitlines()

    def recurse(curr_str, p1, p2, components, depth):
        global strongest, longest
        curr_str += int(p1) + int(p2)

        if depth > longest:
            strongest = curr_str
            longest = depth
            print(depth, strongest)
        elif depth == longest:
            if curr_str > strongest:
                strongest = curr_str
                print(depth, strongest)

        for c in components:
            ports = c.split('/')
                    
            if p2 in ports:
                avail = components[:]
                avail.remove(c)

                n1, n2 = ports
                if n1 != p2:
                    n1, n2 = n2, n1

                recurse(curr_str, n1, n2, avail, depth + 1)

    for c in components:
        if '0' in c.split('/'):
            avail = components[:]
            avail.remove(c)

            p1, p2 = c.split('/')
            if p1 != '0':
                p1, p2 = p2, p1

            recurse(0, p1, p2, avail, 1)

    print(strongest)

if __name__ == '__main__':
    problem_2()