import numpy as np

def problem_1():

    with open("day22_input.txt", "r") as file:
        grid = file.read()
    grid = grid.splitlines()
    w = len(grid[0])
    h = len(grid)
        
    m = np.zeros((h,w), dtype = int)
    for y in range(h):
        for x in range(w):
            c = grid[y][x]
            if c == '#':
                m[y,x] = 1

    dir = (0,-1)
    curr_pos = (w // 2, h // 2)
    cnt = 0
    for burst in range(10000):

        cx, cy = curr_pos

        ci = m[cy,cx]
        if ci == 0:
            cnt += 1

        if ci == 1:
            if dir == (0,-1):
                dir = (1,0)
            elif dir == (0,1):
                dir = (-1,0)
            elif dir == (1,0):
                dir = (0,1)
            elif dir == (-1,0):
                dir = (0,-1)
        else:
            if dir == (0,-1):
                dir = (-1,0)
            elif dir == (0,1):
                dir = (1,0)
            elif dir == (1,0):
                dir = (0,-1)
            elif dir == (-1,0):
                dir = (0,1)

        m[cy,cx] ^= 1

        cx, cy = (cx + dir[0], cy + dir[1])

        if cy < 0:
            m = np.concatenate((np.zeros((1, m.shape[1]), dtype = int), m), axis = 0)
            cy = 0
        elif cy == m.shape[0]:
            m = np.concatenate((m, np.zeros((1, m.shape[1]), dtype = int)), axis = 0)

        if cx < 0:
            m = np.concatenate((np.zeros((m.shape[0], 1), dtype = int), m), axis = 1)
            cx = 0
        elif cx == m.shape[1]:
            m = np.concatenate((m, np.zeros((m.shape[0], 1), dtype = int)), axis = 1)

        curr_pos = (cx, cy)

    print(cnt)

def problem_2():

    with open("day22_input.txt", "r") as file:
        grid = file.read()
    grid = grid.splitlines()
    w = len(grid[0])
    h = len(grid)
        
    #status = [0,1,2,3] # clean, weakened, infected, flagged

    m = np.zeros((h,w), dtype = int)
    for y in range(h):
        for x in range(w):
            c = grid[y][x]
            if c == '#':
                m[y,x] = 2

    dir = (0,-1)
    curr_pos = (w // 2, h // 2)
    cnt = 0
    for burst in range(10000000):

        cx, cy = curr_pos
        ci = m[cy,cx]

        if ci == 2:
            if dir == (0,-1):
                dir = (1,0)
            elif dir == (0,1):
                dir = (-1,0)
            elif dir == (1,0):
                dir = (0,1)
            elif dir == (-1,0):
                dir = (0,-1)
        elif ci == 0:
            if dir == (0,-1):
                dir = (-1,0)
            elif dir == (0,1):
                dir = (1,0)
            elif dir == (1,0):
                dir = (0,-1)
            elif dir == (-1,0):
                dir = (0,1)
        elif ci == 3:
            dir = (-dir[0], -dir[1])

        m[cy,cx] = (ci + 1) % 4
        if m[cy,cx] == 2:
            cnt += 1

        cx, cy = (cx + dir[0], cy + dir[1])

        if cy < 0:
            m = np.concatenate((np.zeros((1, m.shape[1]), dtype = int), m), axis = 0)
            cy = 0
        elif cy == m.shape[0]:
            m = np.concatenate((m, np.zeros((1, m.shape[1]), dtype = int)), axis = 0)

        if cx < 0:
            m = np.concatenate((np.zeros((m.shape[0], 1), dtype = int), m), axis = 1)
            cx = 0
        elif cx == m.shape[1]:
            m = np.concatenate((m, np.zeros((m.shape[0], 1), dtype = int)), axis = 1)

        curr_pos = (cx, cy)

    print(cnt)

if __name__ == '__main__':
    problem_1()
    problem_2()