import re


#p=<-317,1413,1507>, v=<19,-102,-108>, a=<1,-3,-3>

def problem_1():
    rx = re.compile('\d+')
    acc = []
    with open("day20_input.txt", "r") as data_file:
        for row in data_file:
            _,_,a = row.strip().split('>,')
            acc.append(sum([int(x) for x in rx.findall(a)]))

    print(min(enumerate(acc), key = lambda x: x[1]))

def problem_2():
    rx = re.compile('-?\d+')

    particles = []
    with open("day20_input.txt", "r") as data_file:
        for row in data_file:
            p,v,a = row.strip().split('>,')
            p = tuple(int(x) for x in rx.findall(p))
            v = tuple(int(x) for x in rx.findall(v))
            a = tuple(int(x) for x in rx.findall(a))

            particles.append([p, v, a])

    def resolve(particles):
        keys = {}
        for p in particles:
            if p[0] in keys:
                keys[p[0]] = False
            else:
                keys[p[0]] = True

        return list(filter(lambda x: keys[x[0]], particles))

    def update(particles):
        for i,p in enumerate(particles):
            np = p
            np[1] = tuple(map(sum, zip(np[1],np[2]))) 
            np[0] = tuple(map(sum, zip(np[0],np[1]))) 
            particles[i] = np
        return particles

    for i in range(1000):
        particles = resolve(particles)
        particles = update(particles)

    print(len(particles))

if __name__ == '__main__':
    problem_1()
    problem_2()