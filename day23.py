import math

num_mul = 0

def problem_1():

    program = []
    ip = 0
    regs = dict([(chr(x), 0) for x in range(ord('a'), ord('h') + 1)])
    with open("day23_input.txt", "r") as data_file:
        for line in data_file:
            instr, op_a, *op_b = line.strip('\n').split(' ')
            program.append((instr, op_a, op_b))

    def f_set(x, y):
        if y in regs:
            y = regs[y]
        regs[x] = int(y)
        return 1

    def f_sub(x, y):
        if y in regs:
            y = regs[y]
        regs[x] -= int(y)
        return 1

    def f_mul(x, y):
        global num_mul
        if y in regs:
            y = regs[y]
        regs[x] *= int(y)
        num_mul += 1
        return 1

    def f_jnz(x, y):
        if x not in regs:
            if x == 0:
                return 1
            else:
                return int(y)
        if regs[x] == 0:
            return 1
        return int(y)

    f_map = dict([('set', f_set),
                  ('sub', f_sub),
                  ('mul', f_mul),
                  ('jnz', f_jnz)])

    while ip >= 0 and ip < len(program):
        instr, op_a, op_b = program[ip]
        if len(op_b) > 0:
            op_b = op_b[0]
        ip += f_map[instr](op_a, op_b)

    print(num_mul)

def problem_2():
    b = 65 * 100 + 100000
    c = b + 17000

    h = 0
    while 1:
        f = 1
        d = 2
        while 1:
            if b % d == 0:
                f = 0
                break
            d +=1

            if d == b:
                break

        if f == 0:
            h += 1

        if b == c:
            break
        b += 17

    print(h)

if __name__ == '__main__':
    problem_1()
    problem_2()